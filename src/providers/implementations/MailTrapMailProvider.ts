import { MailProvider, Message } from "../MailProvider";
import nodemailer from "nodemailer";
export class MailTrapMailProvider implements MailProvider {
  private transporter;
  constructor() {
    this.transporter = nodemailer.createTransport({
      host: "54.158.84.126",
      port: 2525,
      auth: {
        user: "af51c80e54fd65",
        pass: "c518ec82c2851c",
      },tls: {
        servername: 'smtp.mailtrap.io'
    }
    });
  }
  async sendMail(message: Message): Promise<void> {
    await this.transporter.sendMail({
      to: {
        name: message.to.name,
        address: message.to.email,
      },
      from: {
        name: message.from.name,
        address: message.from.email,
      },
      subject: message.subject,
      html: message.body,
    });
  }
}
